package week1.day1;

public class MobileDetails {
	
	public static void main(String[] args)
	{
		Mobile newMobile = new Mobile();
		newMobile.myMobile();
		String model = newMobile.getModel();
		System.out.println(model);
		boolean hasBattery = newMobile.hasBattery();
		System.out.println(hasBattery);
		int rupees = newMobile.rupees;
		System.out.println(rupees);
		String name = newMobile.name;
		System.out.println(name);
		System.out.println(newMobile.getModel("mm"));
		System.out.println(newMobile.getName("Samsung"));
		
	}
}
